﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class ClassPrac
    {
        string _firstName;
        string _lastName;

        public ClassPrac(string firstName, string LastName)
        {
            this._firstName = firstName;
            this._lastName = LastName;
        }

        public ClassPrac() : this("No FirstName Provided", "No LastName Provided")
        {

        }

        public void PrintFullName()
        {
            Console.WriteLine("Full Name = {0}", this._firstName + " " + this._lastName);
        }

        ~ClassPrac()
        {
            // Clean up code in the class destructor
            // The garbage collector is an automated thing that
            // cleans the our memory of used objects for us
        }
    }
}
