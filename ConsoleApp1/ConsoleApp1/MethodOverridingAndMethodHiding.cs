﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class MethodOverridingAndMethodHiding
    {
        public virtual void Print()
        {
            Console.WriteLine("I am a base class print method");
        }

        public virtual void Print2()
        {
            Console.WriteLine("Another base class method");
        }
    }

    public class DerivedClass : MethodOverridingAndMethodHiding
    {
        public override void Print()
        {            
            Console.WriteLine("I am the derived class");
        }

        public new void Print2()
        {
            Console.WriteLine("Another derived classs method");
        }
    }
}
