﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class InnerException
    {
        public void Test()
        {
            try
            {
                Console.WriteLine("Enter a number");
                int Response = Convert.ToInt32(Console.ReadLine());
            }
            catch
            {
                throw new ArgumentException();
            }
        }
    }
}
