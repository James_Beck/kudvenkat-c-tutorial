﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class NullableTypes
    {
        public static string BoolNull()
        {
            int? i = null;
            bool? AreYouMajor = null;

            if(AreYouMajor == true)
            {
                return "User is Major";
            }
            else if (AreYouMajor == false)
            {
                return "User is not a Major";
            }
            else
            {
                return "User did not answer the question";
            }
        }

        public static int Tickets()
        {
            int? TicketsOnSale = null;
            int AvailableTickets = TicketsOnSale ?? 0;

            return AvailableTickets;
        }
    }
}
