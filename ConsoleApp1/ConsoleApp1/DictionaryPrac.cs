﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class DictionaryPrac
    {
        Person person1 = new Person()
        {
            Id = 2,
            Name = "James",
            Food = "Spaghetti Bolognaise"
        };

        Person person2 = new Person()
        {
            Id = 3,
            Name = "James",
            Food = "Pizza"
        };

        Person person3 = new Person()
        {
            Id = 5,
            Name = "James",
            Food = "Jalepeno Poppers"
        };

        public void Test()
        {
            Dictionary<int, Person> dictJames = new Dictionary<int, Person>();
            dictJames.Add(person1.Id, person1);
            dictJames.Add(person2.Id, person2);
            dictJames.Add(person3.Id, person3);

            foreach (var key in dictJames)
            {
                Console.WriteLine(key.Key);
                Console.WriteLine(key.Value.Name + " " + key.Value.Food);
            }

            foreach (var key in dictJames.Keys)
            {
                Console.WriteLine(key);
            }

            foreach (var value in dictJames.Values)
            {
                Console.WriteLine("{0} - {1} - {2}", value.Id, value.Name, value.Food);
            }

            Console.WriteLine(dictJames[2].Name + " " + dictJames[2].Food);
            Console.WriteLine(dictJames[3].Name + " " + dictJames[3].Food);
            Console.WriteLine(dictJames[5].Name + " " + dictJames[5].Food);

            Person person;
            Console.WriteLine(dictJames.TryGetValue(2, out person));
            Console.WriteLine(dictJames.TryGetValue(3, out person));
            Console.WriteLine(dictJames.TryGetValue(4, out person));
            Console.WriteLine(dictJames.TryGetValue(5, out person));

            Console.WriteLine("Total items = {0}", dictJames.Count(kvp => kvp.Key > 2));
        }
    }

    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Food { get; set; }
    }
}
