﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class QueuePrac
    {
        public void Test()
        {
            Queue<string> queueText = new Queue<string>();
            queueText.Enqueue("Hello");
            queueText.Enqueue("world");
            queueText.Enqueue("this");
            queueText.Enqueue("is");
            queueText.Enqueue("me.");

            foreach (string x in queueText)
            {
                Console.WriteLine(x);
            }
            Console.WriteLine(queueText.Count);

            queueText.Dequeue();
            foreach (string x in queueText)
            {
                Console.WriteLine(x);
            }
            Console.WriteLine(queueText.Count);

            Console.WriteLine(queueText.Peek());
        }
    }
}
