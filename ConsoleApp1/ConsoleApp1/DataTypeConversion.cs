﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class DataTypeConversion
    {
        public static int ConvertFloatToInt()
        {
            float F = 123.45F;
            int I = (int)F;

            return I;
        }

        public static int ConvertClass()
        {
            float F = 123.45F;
            int I = Convert.ToInt32(F);

            return I;
        }

        public static int ParseConvert()
        {
            string Number = "100";
            int I = int.Parse(Number);

            return I;
        }
    }
}
