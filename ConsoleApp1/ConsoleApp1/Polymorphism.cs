﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class Polymorphism
    {
        public string FirstName = "James";
        public string LastName = "Beck";

        public virtual void PrintFullName()
        {
            Console.WriteLine(FirstName + " " + LastName);
        }
    }

    public class PartTimeEmployee : Polymorphism
    {
        public override void PrintFullName()
        {
            Console.WriteLine(FirstName + " " + LastName + " Part Time");
        }
    }

    public class FullTimeEmployee : Polymorphism
    {
        public override void PrintFullName()
        {
            Console.WriteLine(FirstName + " " + LastName + " Full Time");
        }
    }

    public class TemporaryEmployee : Polymorphism
    {
        public override void PrintFullName()
        {
            Console.WriteLine(FirstName + " " + LastName + " Temp");
        }
    }
}
