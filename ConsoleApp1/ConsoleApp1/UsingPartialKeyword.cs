﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public partial class UsingPartialKeyword
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public partial class UsingPartialKeyword
    {
        public string OutputName()
        {
            return FirstName + " " + LastName;
        }
    }
}
