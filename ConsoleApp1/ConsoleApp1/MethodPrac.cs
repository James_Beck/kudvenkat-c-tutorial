﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class MethodPrac
    {
        public void EvenNumbers()
        {
            int Start = 0;

            while (Start <= 20)
            {
                Console.WriteLine(Start);
                Start += 2;
            }
        }

        public int Add(int a, int b)
        {
            return a + b;
        }
    }
}
