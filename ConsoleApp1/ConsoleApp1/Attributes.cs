﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class Attributes
    {
        [Obsolete]
        public int Add(int num1, int num2)
        {
            return num1 + num2;
        }

        [Obsolete("This is obsolete now")]
        public int Add2(int num1, int num2)
        {
            return num1 + num2;
        }

        [Obsolete("This is obsolete now", true)]
        public int Add3(int num1, int num2)
        {
            return num1 + num2;
        }
    }
}
