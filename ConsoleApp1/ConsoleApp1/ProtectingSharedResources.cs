﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace CSharpTutorial
{
    public class ProtectingSharedResources
    {
        public static int Total = 0;
        public static int Total2 = 0;
        public static int Total3 = 0;

        public void Test()
        {
            Thread T1 = new Thread(AddOneMillion);
            Thread T2 = new Thread(AddOneMillion);
            Thread T3 = new Thread(AddOneMillion);

            T1.Start();
            T2.Start();
            T3.Start();

            T1.Join();
            T2.Join();
            T3.Join();

            Console.WriteLine("The total is {0}", Total);

            T1 = new Thread(AddOneMillionInterLocked);
            T2 = new Thread(AddOneMillionInterLocked);
            T3 = new Thread(AddOneMillionInterLocked);

            T1.Start();
            T2.Start();
            T3.Start();

            T1.Join();
            T2.Join();
            T3.Join();

            Console.WriteLine("The total2 is {0}", Total2);

            T1 = new Thread(AddOneMillionLocked);
            T2 = new Thread(AddOneMillionLocked);
            T3 = new Thread(AddOneMillionLocked);

            T1.Start();
            T2.Start();
            T3.Start();

            T1.Join();
            T2.Join();
            T3.Join();

            Console.WriteLine("The total3 is {0}", Total3);
        }

        public void AddOneMillion()
        {
            // Not thread safe
            for (int i = 1; i < 1000001; i++)
            {
                Total++;
            }
        }

        public void AddOneMillionInterLocked()
        {
            // Thread safe
            for (int i = 1; i < 1000001; i++)
            {
                Interlocked.Increment(ref Total2);
            }
        }

        static object _lock = new object();

        public void AddOneMillionLocked()
        {
            // Thread safe
            for (int i = 1; i < 1000001; i++)
            {
                lock (_lock)
                {
                    Total3++;
                }
            }
        }
    }
}
