﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class OptionalParameters
    {
        public void AddNumbers(int number1, int number2, params int[] restOfNumbers)
        {
            int result = number1 + number2;

            if (restOfNumbers != null)
            {
                foreach (int num in restOfNumbers)
                {
                    result += num;
                }
            }

            Console.WriteLine("The sum is equal to {0}", result);
        }

        public void AddNumbers(int number1, int number2)
        {
            Console.WriteLine("The sum is equal to {0}", number1 + number2);
        }

        public void AddNumbers(params int[] numbers)
        {
            int result = 0;

            foreach (int x in numbers)
            {
                result += x;
            }

            Console.WriteLine("The sum is equal to {0}", result);
        }

        public void Test(int number1, int number2 = 10, int number3 = 10)
        {
            Console.WriteLine(number1);
            Console.WriteLine(number2);
            Console.WriteLine(number3);
        }

        public void AddNumbers(int number1, int number2, int number4, [Optional] int number3)
        {
            if (number3 != 0)
            {
                Console.WriteLine(number1 + number2 + number3);
            }
            else
            {
                Console.WriteLine(number1 + number2);
            }
        }
    }
}
