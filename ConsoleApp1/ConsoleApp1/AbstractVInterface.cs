﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public abstract class RapStar : AnyInheriter, IOnlyInterface
    {
        public void Print2()
        {
            Console.WriteLine("Rap Style: Above Reproach");
        }
    }

    public abstract class AnyInheriter
    {
        public void Print()
        {
            Console.WriteLine("Name: Facial Jay");
        }
    }

    public interface IOnlyInterface
    {
        void Print2();
    }

    public interface IRapStar : IOnlyInterface
    {
        
    }

    public class AbstractVInterface : RapStar
    {
        
    }
}
