﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class Delegates
    {
        public void Hello(string message)
        {
            Console.WriteLine(message);
        }

        public void MainDelegate()
        {
            HelloFunctionDelegate Delegate = new HelloFunctionDelegate(Hello);
            Delegate("Hello World");
        }
    }

    public delegate void HelloFunctionDelegate(string message);
}
