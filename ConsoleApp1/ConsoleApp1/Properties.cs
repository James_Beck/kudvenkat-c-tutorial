﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class WhyProperties
    {
        public int Id;
        public string Name;
        public int PassMark = 35;
    }

    public class Properties
    {
        private int _Id;
        private string _Name;
        private int _PassMark = 35;

        public void SetId(int id)
        {
            if (id <= 0)
            {
                throw new Exception("Student Id cannot be negative");
            }

            this._Id = id;
        }

        public int GetId()
        {
            return this._Id;
        }

        public void SetName(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new Exception("Name cannot be null or empty");
            }

            this._Name = name;
        }

        public string GetName()
        {
            return this._Name;
        }

        public int GetPassMark()
        {
            return this._PassMark;
        }
    }
}
