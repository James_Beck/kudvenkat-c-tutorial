﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public interface IInterfacePrac
    {
        void InterfaceMethod();
    }

    public interface IInterfacePrac2
    {
        void InterfaceMethod();
    }

    public class InterfacePrac : IInterfacePrac, IInterfacePrac2
    {
        void IInterfacePrac.InterfaceMethod()
        {
            Console.WriteLine("Method 1");
        }

        void IInterfacePrac2.InterfaceMethod()
        {
            Console.WriteLine("Method 2");
        }
    }
}
