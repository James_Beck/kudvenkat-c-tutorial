﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class MethodOverloading
    {
        public void Add(int x, int y)
        {
            Console.WriteLine(x + y);
        }

        public void Add(int x, int y, int z)
        {
            Console.WriteLine(x + y + z);
        }

        public void Add(int w, int x, int y, int z)
        {
            Console.WriteLine(w + x + y + z);
        }

        public void Add(float x, float y)
        {
            Console.WriteLine(x + y);
        }

        public void Add(int x, int y, out int z)
        {
            z = x + y;
            Console.WriteLine(z);
        }
    }
}
