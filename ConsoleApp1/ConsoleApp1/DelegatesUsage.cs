﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class DelegatesUsage
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Salary { get; set; }
        public int Experience { get; set; }

        public static void PromotedEmployee(List<DelegatesUsage> EmployeeList)
        {
            foreach (DelegatesUsage Employee in EmployeeList)
            {
                if (Employee.Experience > 5)
                {
                    Console.WriteLine(Employee.Name + " promoted");
                }
            }
        }
    }

    public delegate bool IsPromotable(DelegatesUsage2 Test);

    public class DelegatesUsage2
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Salary { get; set; }
        public int Experience { get; set; }

        public static void PromotedEmployee(List<DelegatesUsage2> EmployeeList, IsPromotable IsEligibleToPromote)
        {
            foreach (DelegatesUsage2 Employee in EmployeeList)
            {
                if (IsEligibleToPromote(Employee))
                {
                    Console.WriteLine(Employee.Name + " promoted");
                }
            }
        }
    }
}
