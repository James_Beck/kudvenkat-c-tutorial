﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public abstract class Abstract
    {
        public abstract void Print();
    }

    public abstract class Abstract2
    {
        public void Print()
        {
            Console.WriteLine("Hello Again World");
        }
    }

    public class AbstractImplemented : Abstract
    {
        public override void Print()
        {
            Console.WriteLine("Hello World");
        }
    }

    public class AbstractImplemented2 : Abstract2
    {

    }
}
