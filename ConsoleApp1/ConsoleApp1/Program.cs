﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(NullableTypes.BoolNull());
            Console.WriteLine(NullableTypes.Tickets());
            Console.Clear();

            Console.WriteLine(DataTypeConversion.ConvertFloatToInt());
            Console.WriteLine(DataTypeConversion.ConvertClass());
            Console.WriteLine(DataTypeConversion.ParseConvert());
            Console.Clear();

            foreach (int x in Arrays.NumbersFilter())
            {
                Console.WriteLine(x);
            }
            Console.Clear();

            Console.WriteLine(Switches.SwitchOne(10));
            Console.WriteLine(Switches.SwitchTwo(2));
            Console.Clear();

            Loops L = new Loops();
            L.While();
            L.DoWhile();
            L.For();
            L.ForEach();
            Console.Clear();

            MethodPrac Mp = new MethodPrac();
            Mp.EvenNumbers();
            Console.WriteLine(Mp.Add(1, 8));
            Console.Clear();

            ClassPrac CP1 = new ClassPrac();
            CP1.PrintFullName();
            ClassPrac CP2 = new ClassPrac("James", "Beck");
            CP2.PrintFullName();
            Console.Clear();

            StaticAndInstance SAI = new StaticAndInstance(6);
            Console.WriteLine(SAI.CalculateArea());
            SAI.PrintCircumference(6);
            Console.Clear();

            WithFavouriteFood WFF = new WithFavouriteFood();
            WFF.FirstName = "James";
            WFF.LastName = "Beck";
            WFF.Email = "JB@gmail.com";
            WFF.FavouriteFood = "Spaghetti Bolognaise";
            // This class has Console.WriteLIne methods build into it
            // so when it's called it just outputs those actions as text
            OverrideMessage OM = new OverrideMessage();
            Console.Clear();

            Inheriter1 I1 = new Inheriter1();
            I1.FirstName = "James";
            I1.LastName = "Beck";
            I1.PrintFullName();
            Inheriter2 I2 = new Inheriter2();
            I2.FirstName = "James";
            I2.LastName = "Beck";
            I2.PrintFullName();
            Console.Clear();

            Polymorphism[] PArr = new Polymorphism[4];
            PArr[0] = new Polymorphism();
            PArr[1] = new FullTimeEmployee();
            PArr[2] = new PartTimeEmployee();
            PArr[3] = new TemporaryEmployee();

            foreach (Polymorphism Employee in PArr)
            {
                Employee.PrintFullName();
            }
            Console.Clear();

            MethodOverridingAndMethodHiding MOAMH = new MethodOverridingAndMethodHiding();
            DerivedClass DC = new DerivedClass();
            MethodOverridingAndMethodHiding MOAMH2 = new DerivedClass();
            MOAMH.Print();
            DC.Print();
            MOAMH2.Print();
            MOAMH.Print2();
            DC.Print2();
            MOAMH2.Print2();
            Console.Clear();

            MethodOverloading MO = new MethodOverloading();
            MO.Add(1, 2);
            MO.Add(1, 2, 3);
            MO.Add(1, 2, 3, 4);
            MO.Add(1.5F, 2.5F);
            MO.Add(1, 8);
            Console.Clear();

            WhyProperties WP = new WhyProperties();
            WP.Id = -500;
            WP.Name = null;
            WP.PassMark = 0;
            Console.WriteLine("{0} is person {1} who passed with {2}", WP.Id, WP.Name, WP.PassMark);
            Properties P = new Properties();
            P.SetId(1);
            P.SetName("James");
            Console.WriteLine("{0} is person {1} who passed with {2}", P.GetId(), P.GetName(), P.GetPassMark());
            Console.Clear();

            Properties2 P2 = new Properties2();
            P2.Id = 10;
            P2.Name = "James";
            Console.WriteLine(P2.Id + " " + P2.Name + " " + P2.PassMark);
            Console.Clear();

            Struct S = new Struct(10, "James");
            S.PrintDetails();
            S.Id = 1000;
            S.Name = "Jamelia";
            S.PrintDetails();
            Struct S2 = new Struct
            {
                Id = 1,
                Name = "Timathon"
            };
            S2.PrintDetails();
            Console.Clear();

            Customer C = new Customer();
            C.Print();
            C.CustomerName();
            Console.Clear();

            InterfacePrac IP = new InterfacePrac();
            ((IInterfacePrac)IP).InterfaceMethod();
            ((IInterfacePrac2)IP).InterfaceMethod();
            Console.Clear();

            AbstractImplemented AI = new AbstractImplemented();
            AbstractImplemented2 AI2 = new AbstractImplemented2();
            AI.Print();
            AI2.Print();
            Console.Clear();

            AbstractVInterface AVI = new AbstractVInterface();
            AVI.Print();
            AVI.Print2();
            Console.Clear();

            Delegates Del = new Delegates();
            Del.MainDelegate();
            Console.Clear();

            List<DelegatesUsage> EmployeeList = new List<DelegatesUsage>()
            {
            new DelegatesUsage { Id = 1, Name = "James", Salary = 10000, Experience = 6 },
            new DelegatesUsage { Id = 1, Name = "Rhiannon", Salary = 7000, Experience = 5 },
            new DelegatesUsage { Id = 1, Name = "Jon", Salary = 12000, Experience = 8 },
            new DelegatesUsage { Id = 1, Name = "George", Salary = 5000, Experience = 2 },
            };
            DelegatesUsage.PromotedEmployee(EmployeeList);
            Console.Clear();

            List<DelegatesUsage2> EmployeeList2 = new List<DelegatesUsage2>()
            {
            new DelegatesUsage2 { Id = 1, Name = "James", Salary = 10000, Experience = 6 },
            new DelegatesUsage2 { Id = 1, Name = "Rhiannon", Salary = 7000, Experience = 5 },
            new DelegatesUsage2 { Id = 1, Name = "Jon", Salary = 12000, Experience = 8 },
            new DelegatesUsage2 { Id = 1, Name = "George", Salary = 5000, Experience = 2 },
            };
            DelegatesUsage2.PromotedEmployee(EmployeeList2, emp => emp.Experience > 5);
            Console.Clear();

            SampleDelegate SD1, SD2, SD3, SD4, SD5;
            SD1 = new SampleDelegate(MulticastDelegates.SampleMethodOne);
            SD2 = new SampleDelegate(MulticastDelegates.SampleMethodTwo);
            SD3 = new SampleDelegate(MulticastDelegates.SampleMethodThree);
            SD4 = SD1 + SD2 + SD3;
            SD5 = SD4 - SD2;
            SD1();
            SD2();
            SD3();
            Console.WriteLine();
            SD4();
            Console.WriteLine();
            SD5();
            Console.Clear();

            ExceptionHandling EH = new ExceptionHandling();
            Console.WriteLine(EH.intisone(1));
            Console.WriteLine(EH.intisone(10));
            Console.Clear();

            //InnerException IE = new InnerException();
            //IE.Test();
            //Console.Clear();

            try
            {
                throw new CustomerException();
                throw new CustomerException("Test");
            }
            catch (CustomerException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.InnerException);
            }
            Console.Clear();

            //ExceptionHandlingAbuse EHA = new ExceptionHandlingAbuse();
            //EHA.TestException();
            //Console.Clear();

            //ExceptionHandlingAbuseSolved EHAS = new ExceptionHandlingAbuseSolved();
            //EHAS.TestException();
            //Console.Clear();

            EnumExample EE = new EnumExample();
            EE.RespondPlease();
            Console.Clear();

            EnumExample2 EE2 = new EnumExample2();
            EE2.PleaseRespond();
            Console.Clear();

            PrivateAndPublic PAP = new PrivateAndPublic();
            Console.WriteLine(PAP.youCanAccessThis(10));
            Console.WriteLine(PAP.butYouCanUseThisToAccessThePrivateOne(10));
            Console.Clear();

            Protected Pr = new Protected();
            Console.WriteLine(Pr.youCanAccessThis(100));
            Console.WriteLine(Pr.butYouCanUseThisToAccessThePrivateOne(100));
            Console.Clear();

            UsingInternalAccessModifier UIAM = new UsingInternalAccessModifier();
            UIAM.Test();
            UsingProtectedInternalAccessModifier UPIAM = new UsingProtectedInternalAccessModifier();
            UPIAM.ReturnProtected();
            Console.Clear();

            Attributes A = new Attributes();
            A.Add(1, 1);
            A.Add2(1, 1);
            //A.Add3(1, 1); This will throw an error normally
            Console.Clear();

            Reflection R = new Reflection();
            R.Test();
            Console.Clear();

            EarlyBinding EB = new EarlyBinding();
            EB.Test();
            LateBinding LB = new LateBinding();
            LB.Test();
            Console.Clear();

            Generics G = new Generics();
            G.Comparer(10, 10);
            G.Comparer("AB", "BC");
            G.Comparer("AB", "AB");
            Console.Clear();

            OverridingToString OTS = new OverridingToString();
            OTS.FirstName = "James";
            OTS.LastName = "Beck";
            Console.WriteLine(Convert.ToString(OTS));
            Console.Clear();

            OverridingEquals OE1 = new OverridingEquals();
            OE1.FirstName = "James";
            OE1.LastName = "Beck";
            OverridingEquals OE2 = new OverridingEquals();
            OE2.FirstName = "James";
            OE2.LastName = "Beck";
            Console.WriteLine(OE1 == OE2);
            Console.WriteLine(OE1.Equals(OE2));
            Console.Clear();

            UsingStringBuilder USB = new UsingStringBuilder();
            USB.Test();
            Console.Clear();

            UsingPartialKeyword UPK = new UsingPartialKeyword();
            UPK.FirstName = "James";
            UPK.LastName = "Beck";
            Console.WriteLine(UPK.OutputName());
            Console.Clear();

            OptionalParameters OP = new OptionalParameters();
            OP.AddNumbers(1, 2);
            OP.AddNumbers(1, 2, 3);
            OP.AddNumbers(1, 2, new int[] { 3, 4, 5, 6, 7, 8, 9, 10 });
            OP.AddNumbers(new int[] { 1, 2, 3, 4, 5 });
            OP.Test(1, 1, 2);
            OP.Test(number1: 1, number3: 5, number2: 8);
            OP.Test(10);
            OP.AddNumbers(1, 2, 3);
            OP.AddNumbers(1, 4, 7, 9);
            Console.Clear();

            DictionaryPrac DP = new DictionaryPrac();
            DP.Test();
            Console.Clear();

            ListPrac LP = new ListPrac();
            LP.Test();
            Console.Clear();

            QueuePrac QP = new QueuePrac();
            QP.Test();
            Console.Clear();

            StackPrac SP = new StackPrac();
            SP.Test();
            Console.Clear();

            //MainAndChildThread MACT = new MainAndChildThread();
            //MACT.Test();
            //Console.Clear();

            ThreadJoinAndThreadIsAlive THATIA = new ThreadJoinAndThreadIsAlive();
            THATIA.Test();
            Console.Clear();

            ProtectingSharedResources PSR = new ProtectingSharedResources();
            PSR.Test();
            Console.Clear();

            MultipleThreadingSpeedTest MTST = new MultipleThreadingSpeedTest();
            MTST.Test();
            Console.Clear();


        }
    }
}
