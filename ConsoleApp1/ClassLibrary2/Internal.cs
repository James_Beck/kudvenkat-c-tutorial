﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary2
{
    public class Internal
    {
        internal void Test()
        {
            Console.WriteLine("Hello World");
        }

        public void Test2()
        {
            Console.WriteLine("Hello World Again");
        }
    }
}
