﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary2;

namespace CSharpTutorial
{
    public class UsingInternalAccessModifier
    {
        public void Test()
        {
            Internal I = new Internal();
            //I.Test();
            I.Test2();
        }
    }

    public class UsingProtectedInternalAccessModifier : ProtectedInternal
    {
        public void ReturnProtected()
        {
            base.Test();
        }
    }
}
