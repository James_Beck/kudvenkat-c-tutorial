﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;

namespace CSharpTutorial
{
    public class MultipleThreadingSpeedTest
    {
        public void Test()
        {
            Console.WriteLine(Environment.ProcessorCount);

            Stopwatch stopwatch = Stopwatch.StartNew();
            OddNumberSum();
            EvenNumberSum();
            stopwatch.Stop();
            Console.WriteLine(stopwatch.ElapsedMilliseconds);

            stopwatch = Stopwatch.StartNew();
            Thread T1 = new Thread(OddNumberSum);
            Thread T2 = new Thread(EvenNumberSum);

            T1.Start();
            T2.Start();
            T1.Join();
            T2.Join();
            stopwatch.Stop();
            Console.WriteLine(stopwatch.ElapsedMilliseconds);
        }

        public void OddNumberSum()
        {
            int sum = 0;
            for (int i = 0; i < 500000000; i++)
            {
                if (i % 2 == 1)
                {
                    sum += i;
                }
            }

            Console.WriteLine(sum);
        }

        public void EvenNumberSum()
        {
            int sum = 0;
            for (int i = 0; i < 500000000; i++)
            {
                if (i % 2 == 0)
                {
                    sum += i;
                }
            }

            Console.WriteLine(sum);
        }
    }
}
