﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class HidingMethod
    {
        public string FirstName;
        public string LastName;

        public void PrintFullName()
        {
            Console.WriteLine(FirstName + " " + LastName);
        }
    }

    public class Inheriter1 : HidingMethod
    {
        public new void PrintFullName()
        {
            Console.WriteLine("King " + FirstName + " " + LastName);
        }
    }

    public class Inheriter2 : HidingMethod
    {

    }
}
