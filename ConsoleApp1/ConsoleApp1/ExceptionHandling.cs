﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class ExceptionHandling
    {
        public int intisone(int number)
        {
            
            if (number == 1)
            {
                return 1;
            }
            else
            {
                ArgumentException ex = new ArgumentException();
                Console.WriteLine(ex.Message);

                return 0;
            }
        }
    }
}
