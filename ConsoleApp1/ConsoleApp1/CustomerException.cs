﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    [Serializable]
    public class CustomerException : Exception
    {
        public CustomerException() : base() { }
        public CustomerException(string message) : base(message) { }
        public CustomerException(string message, Exception innerException) : base(message, innerException) { }
        public CustomerException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
