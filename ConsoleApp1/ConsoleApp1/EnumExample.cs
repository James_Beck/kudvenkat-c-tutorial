﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class EnumExample
    {
        public void RespondPlease()
        {
            APerson[] AListOfPeople = new APerson[3];

            AListOfPeople[0] = new APerson
            {
                Name = "James",
                Gender = Gender.Male
            };
            AListOfPeople[1] = new APerson
            {
                Name = "Jamelia",
                Gender = Gender.Female
            };
            AListOfPeople[2] = new APerson
            {
                Name = "Gollum",
                Gender = Gender.Unknown
            };

            foreach(APerson person in AListOfPeople)
            {
                Console.WriteLine("{0} - {1}", person.Name, person.Gender);
            }
        }
    }

    public class APerson
    {
        public string Name { get; set; }
        public Gender Gender { get; set; }
    }

    public enum Gender
    {
        Unknown,
        Male,
        Female
    }
}
