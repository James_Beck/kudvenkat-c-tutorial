﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class Inheritance
    {
        public string FirstName;
        public string LastName;
        public string Email;

        public Inheritance()
        {
            Console.WriteLine(FirstName + " " + LastName);
        }

        public Inheritance(string Message)
        {
            Console.WriteLine(Message);
        }
    }

    public class OverrideMessage : Inheritance
    {
        public OverrideMessage() : base("This is the parent class")
        {
            Console.WriteLine("This is the child class");
        }
    }

    public class WithFavouriteFood : Inheritance
    {
        public string FavouriteFood;
    }
}
