﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class ExceptionHandlingAbuseSolved
    {
        public void TestException()
        {            
            Console.WriteLine("Please enter a number");
            int Numerator;
            bool IsNumeratorANumber = Int32.TryParse(Console.ReadLine(), out Numerator);

            if (IsNumeratorANumber)
            {
                Console.WriteLine("Enter another number");
                int Denominator;
                bool IsDenominatorANumber = Int32.TryParse(Console.ReadLine(), out Denominator);

                if (IsDenominatorANumber && Denominator != 0)
                {
                    Console.WriteLine(Numerator / Denominator);
                }                    
            }
        }
    }
}
