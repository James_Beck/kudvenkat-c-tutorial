﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public struct Struct
    {
        private int _Id;
        private string _Name;

        public int Id
        {
            get { return this._Id; }
            set { this._Id = value; }
        }

        public string Name
        {
            get => _Name;
            set => _Name = value;
        }

        public Struct(int Id, string Name)
        {
            this._Id = Id;
            this._Name = Name;
        }

        public void PrintDetails()
        {
            Console.WriteLine("Id = {0} & Name = {1}", this._Id, this._Name);
        }
    }
}
