﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class Reflection
    {
        public void Test()
        {
            Type T = Type.GetType("CSharpTutorial.TestUnitPerson");
            Console.WriteLine("Full Name = {0}", T.FullName);
            Console.WriteLine("Class Name = {0}", T.Name);
            Console.WriteLine("Namespace = {0}", T.Namespace);

            PropertyInfo[] properties = T.GetProperties();
            
            foreach (PropertyInfo property in properties)
            {
                Console.WriteLine("{0} is of type {1}", property.Name, property.PropertyType.Name);
            }

            MethodInfo[] methods = T.GetMethods();

            foreach (MethodInfo method in methods)
            {
                Console.WriteLine("{0}{1} is a method", method.ReturnType.Name, method.Name);
            }

            ConstructorInfo[] constructors = T.GetConstructors();
            foreach (ConstructorInfo constructor in constructors)
            {
                Console.WriteLine("{0} is a constructor", constructor);
            }
        }
        
    }

    public class TestUnitPerson
    {
        private int _id { get; set; }
        private string _name { get; set; }

        // These ones are for the purpose of highlighting protection levels
        public int visibleId { get; set; }
        public string visableName { get; set; }

        public TestUnitPerson(int id, string name)
        {
            this._id = id;
            this._name = name;
        }

        public void Test()
        {
            Console.WriteLine("The Id is {0} and the Name is {1]", _id, _name);
        }
    }
}
