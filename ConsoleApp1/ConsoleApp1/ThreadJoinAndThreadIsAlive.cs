﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class ThreadJoinAndThreadIsAlive
    {
        public void Test()
        {
            Console.WriteLine("Thread Started");
            Thread T1 = new Thread(Thread1);
            T1.Start();

            Thread T2 = new Thread(Thread2);
            T2.Start();

            T1.Join();
            Console.WriteLine("Thread One Completed");

            T2.Join();
            Console.WriteLine("Thread TWo Completed");

            if (T1.IsAlive)
            {
                Console.WriteLine("Thread one still working");
            }
            else
            {
                Console.WriteLine("THread One completed");
            }

            Console.WriteLine("Completed");
        }

        public void Thread1()
        {
            Console.WriteLine("THis is thread one");
            Thread.Sleep(1000);
        }

        public void Thread2()
        {
            Console.WriteLine("This is thread two");
        }
    }
}
