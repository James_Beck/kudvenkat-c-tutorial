﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ListOfEmployees LOE = new ListOfEmployees();

            Response.Write("The name of the employee with Id 2: " + LOE[2]);
            Response.Write("<br/>");
            Response.Write("The name of the employee with Id 4: " + LOE[4]);
            Response.Write("<br/>");
            Response.Write("The name of the employee with Id 6: " + LOE[6]);
            Response.Write("<br/>");
            Response.Write("<br/>");

            LOE[2] = "Darren";
            LOE[4] = "Darrol";
            LOE[6] = "DarDar";

            Response.Write("The name of the employee with Id 2: " + LOE[2]);
            Response.Write("<br/>");
            Response.Write("The name of the employee with Id 4: " + LOE[4]);
            Response.Write("<br/>");
            Response.Write("The name of the employee with Id 6: " + LOE[6]);
            Response.Write("<br/>");
            Response.Write("<br/>");

            Response.Write("Total number of males: " + LOE["Male"]);
            Response.Write("<br/>");
            Response.Write("Total number of females: " + LOE["Female"]);
            Response.Write("<br/>");
            Response.Write("<br/>");

            LOE["Male"] = "Female";

            Response.Write("Total number of males: " + LOE["Male"]);
            Response.Write("<br/>");
            Response.Write("Total number of females: " + LOE["Female"]);
            Response.Write("<br/>");
        }
    }
}