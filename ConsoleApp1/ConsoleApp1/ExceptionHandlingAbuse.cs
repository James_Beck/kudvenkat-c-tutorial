﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class ExceptionHandlingAbuse
    {
        public void TestException()
        {
            try
            {
                Console.WriteLine("Please enter a number");
                int Numerator = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter another number");
                int Denominator = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine(Numerator / Denominator);
            }
            catch (FormatException)
            {
                Console.WriteLine("Please enter a number");
            }            
            catch (OverflowException)
            {
                Console.WriteLine("Only use numbers between {0} and {1]", Int32.MinValue, Int32.MaxValue);
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("Cannot be a 0");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            // The problem is that your code is extremely faulty
            // There are a lot of different ways that you can make this code better
            // That is always what you should do. Inputs should yield the fewest errors possible
        }
    }
}
