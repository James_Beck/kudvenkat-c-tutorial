﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class EarlyBinding
    {
        public void Test()
        {
            TestBinding TB = new TestBinding();
            string fullname = TB.FullName("James", "Beck");
            Console.WriteLine("Full Name = {0}", fullname);
        }
    }

    public class LateBinding
    {
        public void Test()
        {
            Assembly executingAssembly = Assembly.GetExecutingAssembly();
            Type testBindingType = executingAssembly.GetType("CSharpTutorial.TestBinding");
            object testBindingInstance = Activator.CreateInstance(testBindingType);
            MethodInfo getFullName = testBindingType.GetMethod("FullName");
            string[] parameters = new string[2] { "James", "Beck" };

            string fullname = getFullName.Invoke(testBindingInstance, parameters).ToString();
            Console.WriteLine("Full Name = {0}", fullname);
        }
    }

    public class TestBinding
    {
        public string FullName(string firstname, string lastname)
        {
            return firstname + " " + lastname;
        }
    }
}
