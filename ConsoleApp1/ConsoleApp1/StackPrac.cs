﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class StackPrac
    {
        public void Test()
        {
            Stack<string> testStack = new Stack<string>();
            testStack.Push("Hello");
            testStack.Push("World");
            testStack.Push("This");
            testStack.Push("Is");
            testStack.Push("Me");

            foreach (string x in testStack)
            {
                Console.WriteLine(x);
            }

            testStack.Pop();
            foreach (string x in testStack)
            {
                Console.WriteLine(x);
            }

            Console.WriteLine(testStack.Peek());
        }
    }
}
