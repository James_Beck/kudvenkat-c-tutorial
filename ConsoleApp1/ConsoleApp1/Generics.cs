﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class Generics
    {
        public void Comparer<T>(T value1, T value2)
        {
            if (value1.Equals(value2))
            {
                Console.WriteLine("The two inputs {0} and {1} are the same", value1, value2);
            }
            else
            {
                Console.WriteLine("The two inputs {0} and {1} are different", value1, value2);
            }
        }
    }
}
