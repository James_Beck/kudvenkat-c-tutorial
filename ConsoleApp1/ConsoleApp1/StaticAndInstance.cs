﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class StaticAndInstance
    {
        static float _Pi = 3.141F;
        int _Radius;

        public StaticAndInstance(int Radius)
        {
            this._Radius = Radius;
        }

        public static void Circumference(int Radius)
        {
            Console.WriteLine(_Pi * (Radius * 2));
        }

        // This is created becasuse the public static void Circumference
        // Couldn't be called from created an instance of the class
        public void PrintCircumference(int Radius)
        {
            Circumference(Radius);
        }

        public float CalculateArea()
        {
            return _Pi * (float)Math.Pow(this._Radius, 2);
        }
    }
}
