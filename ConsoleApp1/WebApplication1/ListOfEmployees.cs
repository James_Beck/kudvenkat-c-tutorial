﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication1
{
    public class ListOfEmployees
    {
        private List<EmployeeModel> _listEmployees;

        public ListOfEmployees()
        {
            _listEmployees = new List<EmployeeModel>
            {
                new EmployeeModel {Id = 1, Name = "James", Gender = "Male" },
                new EmployeeModel {Id = 2, Name = "Jamelia", Gender = "Male" },
                new EmployeeModel {Id = 3, Name = "Jane", Gender = "Male" },
                new EmployeeModel {Id = 4, Name = "Jame", Gender = "Male" },
                new EmployeeModel {Id = 5, Name = "Jameriquai", Gender = "Male" },
                new EmployeeModel {Id = 6, Name = "Jay", Gender = "Male" }
            };
        }

        public string this[int Id]
        {
            get
            {
                return _listEmployees.FirstOrDefault(emp => emp.Id == Id).Name;
            }
            set
            {
                _listEmployees.FirstOrDefault(emp => emp.Id == Id).Name = value;
            }
        }

        public string this[string Gender]
        {
            get
            {
                return _listEmployees.Count(emp => emp.Gender == Gender).ToString();
            }
            set
            {
                foreach (EmployeeModel Employee in _listEmployees)
                {
                    if (Employee.Gender == Gender)
                    {
                        Employee.Gender = value;
                    }
                }
            }
        }
    }

    public class EmployeeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
    }
}
