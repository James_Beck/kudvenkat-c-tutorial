﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public interface ICustomer
    {
        void Print();
    }

    public interface ICustomer2 : ICustomer
    {
        void CustomerName();
    }

    class Customer : ICustomer2
    {
        public void Print()
        {
            Console.WriteLine("This uses an interface");
        }

        public void CustomerName()
        {
            Console.WriteLine("Facial Jay");
        }
    }
}
