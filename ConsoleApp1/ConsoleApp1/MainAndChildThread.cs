﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public delegate void SumOfNumbersCallback(int SumOfNumbers);

    public class MainAndChildThread
    {
        public void Test()
        {
            Console.WriteLine("Please enter a target number greater than 0");
            int target = Convert.ToInt32(Console.ReadLine());

            SumOfNumbersCallback callback = new SumOfNumbersCallback(PrintSum);
            Number number = new Number(target, callback);
            Thread childThread = new Thread(new ThreadStart(number.PrintSumOfNumbers));
            childThread.Start();
        }

        public void PrintSum(int sum)
        {
            Console.WriteLine("Sum of numbers = {0}", sum);
        }
    }

    public class Number
    {
        int _target;
        SumOfNumbersCallback _callbackMethod;

        public Number(int target, SumOfNumbersCallback callbackMethod)
        {
            _target = target;
            _callbackMethod = callbackMethod;
        }

        public void PrintSumOfNumbers()
        {
            int sum = 0;
            for (int i = 0; i < _target; i++)
            {
                sum += i;
            }

            if (_callbackMethod != null)
            {
                _callbackMethod(sum);

                // or _callbackMethod?.Invoke(sum);
            }
        }
    }
}
