﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class EnumExample2
    {
        public void PleaseRespond()
        {
            int[] values = (int[])Enum.GetValues(typeof(Sex));
            string[] names = Enum.GetNames(typeof(Sex));
            MoreSex more = (MoreSex)Sex.Male;

            foreach (short value in values)
            {
                Console.WriteLine(value);
            }

            foreach (string name in names)
            {
                Console.WriteLine(name);
            }

            Console.WriteLine(more);
        }
    }

    public enum Sex
    {
        Unknown = 10,
        Male = 8,
        Female,
        MegaMan
    }

    public enum MoreSex
    {
        Another,
        AnotherOne,
        AndAnotherOne
    }
}
