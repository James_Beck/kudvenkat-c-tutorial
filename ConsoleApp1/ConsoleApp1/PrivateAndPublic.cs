﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class PrivateAndPublic
    {
        private int youCannotAccessThis(int input)
        {
            return input * 2;
        }

        public int youCanAccessThis(int input)
        {
            return input * 3;
        }

        public int butYouCanUseThisToAccessThePrivateOne(int input)
        {
            return youCannotAccessThis(input);
        }
    }
}
