﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class Loops
    {
        public void While()
        {
            int UserTarget = 10;
            int Start = 0;

            while (Start <= UserTarget)
            {
                Console.WriteLine(Start);
                Start++;
            }
        }

        public void DoWhile()
        {
            int UserTarget = 10;
            int Start = 0;

            do
            {
                Console.WriteLine(Start);
                Start++;
            } while (Start < UserTarget);
        }

        public void For()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(i);
            }
        }
        
        public void ForEach()
        {
            int[] Arr = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            foreach (int x in Arr)
            {
                Console.WriteLine(x);
            }
        }
    }
}
