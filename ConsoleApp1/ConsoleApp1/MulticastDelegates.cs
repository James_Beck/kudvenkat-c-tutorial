﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class MulticastDelegates
    {
        public static void SampleMethodOne()
        {
            Console.WriteLine("One");
        }

        public static void SampleMethodTwo()
        {
            Console.WriteLine("Two");
        }

        public static void SampleMethodThree()
        {
            Console.WriteLine("Three");
        }
    }

    public delegate void SampleDelegate();


}
