﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class ListPrac
    {
        public void Test()
        {
            List<string> testList = new List<string>()
            {
                "Hello",
                "World",
                "This"
            };

            foreach (string text in testList)
            {
                Console.Write(text + " ");
            }
            Console.WriteLine();

            List<string> testList2 = new List<string>()
            {
                "Is",
                "Me"
            };

            testList.AddRange(testList2);

            foreach (string text in testList)
            {
                Console.Write(text + " ");
            }
            Console.WriteLine();

            List<string> testList3 = testList.GetRange(1, 2);

            foreach (string text in testList3)
            {
                Console.Write(text + " ");
            }
            Console.WriteLine();

            List<string> forSorting = new List<string>()
            {
                "A",
                "C",
                "B",
                "E",
                "D"
            };
            
            foreach (string x in forSorting)
            {
                Console.WriteLine(x);
            }
            forSorting.Sort();
            foreach (string x in forSorting)
            {
                Console.WriteLine(x);
            }
            forSorting.Reverse();
            foreach (string x in forSorting)
            {
                Console.WriteLine(x);
            }

            List<Person2> customerList = new List<Person2>()
            {
                new Person2() { Id = 1, Name = "James", Food = "Pizza" },
                new Person2() { Id = 2, Name = "Steve", Food = "Sauce" },
                new Person2() { Id = 3, Name = "Kyle", Food = "Chick Nugs" },
                new Person2() { Id = 4, Name = "Fred", Food = "Chips" },
                new Person2() { Id = 5, Name = "Jane", Food = "Fire" },
            };

            foreach (Person2 person in customerList)
            {
                Console.WriteLine(person.Id + person.Name + person.Food);
            }
            customerList.Sort();
            foreach (Person2 person in customerList)
            {
                Console.WriteLine(person.Id + person.Name + person.Food);
            }
            customerList.Reverse();
            foreach (Person2 person in customerList)
            {
                Console.WriteLine(person.Id + person.Name + person.Food);
            }

            Console.WriteLine(customerList.TrueForAll(x => x.Name.Contains("e")));
            ReadOnlyCollection<Person2> readonlyPeople = customerList.AsReadOnly();
            Console.WriteLine(readonlyPeople[1]);

            List<string> testList4 = new List<string>(100)
            {
                "Testing",
                "Testing",
            };

            Console.WriteLine(testList4.Capacity);
            testList4.TrimExcess();
            Console.WriteLine(testList4.Capacity);

        }
    }

    public class Person2 : IComparable<Person2>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Food { get; set; }

        public int CompareTo(Person2 sorted)
        {
            return Name.CompareTo(sorted.Name);
        }
    }
}
