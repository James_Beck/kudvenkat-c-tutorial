﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class UsingStringBuilder
    {
        public void Test()
        {
            StringBuilder TestString = new StringBuilder("Hello");
            TestString.Append(" World");
            TestString.Append(" This");
            TestString.Append(" Is");
            TestString.Append(" Me");

            Console.WriteLine(TestString);
        }
        
    }
}
