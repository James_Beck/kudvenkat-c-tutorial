﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class Switches
    {
        public static string SwitchOne(int input)
        {
            int UserNumber = input;

            switch(UserNumber)
            {
                case 0: return "Your number is 0";
                case 10: return "Your number is 10";
                case 20: return "Your number is 20";
                case 30: return "Your number is 30";
                case 40: return "Your number is 40";
                case 50: return "Your number is 50";
                default: return "Was looking for 0, 10, 20, 30, 40, or 50";
            }
        }

        public static string SwitchTwo(int input)
        {
            Console.WriteLine("1) Small, 2) Medium, 3) Large");
            int TotalCoffeeCost = 0;

            switch (input)
            {
                case 1: TotalCoffeeCost += 1; break;
                case 2: TotalCoffeeCost += 2; break;
                case 3: TotalCoffeeCost += 3; break;
                default: break;
            }

            return "Your coffee costs $" + TotalCoffeeCost;
        }
    }
}
