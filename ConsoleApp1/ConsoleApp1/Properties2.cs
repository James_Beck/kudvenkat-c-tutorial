﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTutorial
{
    public class Properties2
    {
        private int _Id;
        private string _Name;
        private int _PassMark = 35;
        public string City { get; set; }
        public string Email { get; set; }

        public int Id
        {
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Id cannot be zero of negative");
                }

                this._Id = value;
            }
            get
            {
                return this._Id;
            }
        }

        public string Name
        {
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Name cannot be null or empty");
                }

                this._Name = value;
            }
            get
            {
                return this._Name;
            }
        }

        public int PassMark
        {
            get
            {
                return this._PassMark;
            }
        }
    }
}
